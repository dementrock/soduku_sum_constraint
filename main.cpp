//
// A general purpose Soduku solver with sum constraints
//

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

struct Entry {
    int x, y;
};

struct Constraint {
    int sum;
    vector<Entry*> entries;
};

vector<Constraint*> constraints, constraints_by_entry[9][9];

int used_per_row[9][10], used_per_column[9][10], used_per_square[9][10];

int entry[9][9];

int regions[9][9] = {
    {0, 0, 0, 1, 1, 1, 2, 2, 2},
    {0, 0, 0, 1, 1, 1, 2, 2, 2},
    {0, 0, 0, 1, 1, 1, 2, 2, 2},
    {3, 3, 3, 4, 4, 4, 5, 5, 5},
    {3, 3, 3, 4, 4, 4, 5, 5, 5},
    {3, 3, 3, 4, 4, 4, 5, 5, 5},
    {6, 6, 6, 7, 7, 7, 8, 8, 8},
    {6, 6, 6, 7, 7, 7, 8, 8, 8},
    {6, 6, 6, 7, 7, 7, 8, 8, 8},
};


void parse_constraint(string &line) {
    // states:
    //      0: new entry
    //      1: into first number
    //      2: into second number
    //      3: into sum
    int state = 0; 
    int num = 0;
    int sum = 0;
    vector<int> xs, ys;
    for (int i = 0; i < line.length(); ++i) {
        if (line[i] == ' ' || line[i] == '+' ) {
            continue;
        } else if (line[i] == '=') {
            state = 3;
            num = 0;
        } else if (line[i] == '(') {
            state = 1;
            num = 0;
        } else if (line[i] == ',') {
            state = 2;
            xs.push_back(num - 1);
            num = 0;
        } else if (line[i] == ')') {
            state = 0;
            ys.push_back(num - 1);
            num = 0;
        } else if ('0' <= line[i] && line[i] <= '9') {
            num = num * 10 + (line[i] - '0');
        }
    }
    Constraint *c = new Constraint;
    c->sum = num;
    for (int i = 0; i < xs.size(); ++i) {
        Entry *entry = new Entry;
        entry->x = xs[i];
        entry->y = ys[i];
        c->entries.push_back(entry);
        constraints_by_entry[xs[i]][ys[i]].push_back(c);
    }
    constraints.push_back(c);
}
void parse_input(void) {
    vector<string> input;
    string line;
    while (getline(cin, line)) {
        parse_constraint(line);
    }
}

void print_constraint(void) {
    cout << constraints.size() << endl;
    for (int i = 0; i < constraints.size(); ++i) {
        for (int j = 0; j < constraints[i]->entries.size(); ++j) {
            cout << "(" << constraints[i]->entries[j]->x << "," << constraints[i]->entries[j]->y << ") + ";
        }
        cout << " = " << constraints[i]->sum << endl;
    }
}

bool put(int x, int y, int num) {
    if (used_per_row[x][num] || used_per_column[y][num] || used_per_square[regions[x][y]][num]) {
        return false;
    }
    entry[x][y] = num;
    return used_per_row[x][num] = used_per_column[y][num] = used_per_square[regions[x][y]][num] = true;
}

void unput(int x, int y) {
    used_per_row[x][entry[x][y]] = used_per_column[y][entry[x][y]] = used_per_square[regions[x][y]][entry[x][y]] = false;
    entry[x][y] = 0;
}

bool entry_satisfiable(int x, int y) {
    for (int i = 0; i < constraints_by_entry[x][y].size(); ++i) {
        int sum = 0;
        bool completed = true;
        for (int j = 0; j < constraints_by_entry[x][y][i]->entries.size(); ++j) {
            int xx = constraints_by_entry[x][y][i]->entries[j]->x;
            int yy = constraints_by_entry[x][y][i]->entries[j]->y;
            sum += entry[xx][yy];
            if (sum > constraints_by_entry[x][y][i]->sum) {
                return false;
            }
            if (entry[xx][yy] == 0) {
                completed = false;
            }
        }
        if (completed && sum != constraints_by_entry[x][y][i]->sum) {
            return false;
        }
    }
    return true;
}

void dfs(int x, int y) {
    if (x == 9 && y == 0) {
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                cout << entry[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        return;
    }
    int nx = x, ny = y + 1;
    if (ny >= 9) {
        nx += 1;
        ny = 0;
    }
    for (int i = 1; i <= 9; ++i) {
        if (!put(x, y, i)) {
            continue;
        }
        if (!entry_satisfiable(x, y)) {
            unput(x, y);
            continue;
        }
        dfs(nx, ny);
        unput(x, y);
    }
}

void solve() {
    dfs(0, 0);
}

int main() {
    parse_input();
    solve();
    return 0;
}
